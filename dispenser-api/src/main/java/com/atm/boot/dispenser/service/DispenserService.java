package com.atm.boot.dispenser.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.atm.boot.api.json.Data;
import com.atm.boot.api.json.Result;
import com.atm.boot.entity.Note;
import com.atm.boot.entity.NoteRepository;
import com.atm.boot.utils.Helper;
import com.atm.boot.utils.NotesDTO;
import com.atm.boot.utils.Transformer;
import com.atm.boot.utils.WithdrawDTO;

/**
 * @author georgiep
 *     <p>The API service provides the implementation details of the method exposed by the {@link
 *     DispenserController}
 */
@Service
public class DispenserService {

  private static final int NOTE50 = 50;
  private static final int NOTE20 = 20;

  @Autowired private Transformer transformer;

  @Autowired private NoteRepository noteRepository;

  /**
   * Sets up the dispenser
   *
   * @param notesDTO The notes to setup
   * @return The actual {@link Result}
   */
  public Result setup(final List<NotesDTO> notesDTO) {

    setUpNotes(notesDTO);

    return Helper.constructOKResult(new Data());
  }

  /**
   * The set up of notes includes first deleting and then saving a {@link List} of {@link Note}s
   *
   * @param notesDTO {@link List} of {@link Note}s to set up
   */
  private void setUpNotes(final List<NotesDTO> notesDTO) {
    noteRepository.deleteAll();
    noteRepository.saveAll(transformer.transformNotesDTOToEntity(notesDTO));
  }
  
  /**
   * Gets the actual balance from the dispenser
   *
   * @return The actual {@link Result}
   */
  public Result getBalance() {
    final int balance =
        noteRepository
            .findAllByOrderByNoteAsc()
            .stream()
            .mapToInt(n -> n.getAmount() * n.getNote())
            .sum();
    final List<com.atm.boot.api.json.Note> notesJson =
        transformer.transformNotesEntityToJson((List<Note>) noteRepository.findAll());
    return Helper.constructOKResult(Data.builder().balance(balance).notes(notesJson).build());
  }

  /**
   * Withdraws money from the dispenser. Utilizes the {@link Helper#solutions(int[], int[], int[],
   * int, int)} method
   *
   * @param withdrawDTO The amount to withdraw
   * @return The actual {@link Result}
   */
  public synchronized Result withdraw(final WithdrawDTO withdrawDTO) {

    final List<Note> allNotes = noteRepository.findAllByOrderByNoteAsc();
    final int[] notes = allNotes.stream().mapToInt(n -> n.getNote()).toArray();
    final int[] amounts = allNotes.stream().mapToInt(n -> n.getAmount()).toArray();
    final List<Integer[]> results =
        Helper.solutions(notes, amounts, new int[2], withdrawDTO.getAmount(), 0);
    return decideOnWithdrawnResult(withdrawDTO, results);
  }

  /**
   * Decides whether the amount can be withdrawn or not. In the first case the {@link Note}s are
   * updated.
   *
   * @param withdrawDTO The amount to withdraw
   * @param results The {@link List} of results to check
   * @return The actual {@link Result}
   */
  private Result decideOnWithdrawnResult(
      final WithdrawDTO withdrawDTO, final List<Integer[]> results) {

    if (results.isEmpty())
      return Helper.buildResult(
          new Data(),
          HttpStatus.OK,
          new StringBuilder("Cannot withdraw amount $").append(withdrawDTO.getAmount()).toString());
    
    saveWithTheFirstCombinationOnly(results);

    return Helper.buildResult(
        new Data(),
        HttpStatus.OK,
        new StringBuilder("Amount $")
            .append(withdrawDTO.getAmount())
            .append(" withdrawn successfully")
            .toString());
  }
  
  /**
   * Saves the first combination of notes found
   *
   ** @param results The {@link List} of results to check
   */
  private void saveWithTheFirstCombinationOnly(final List<Integer[]> results) {
		for (final Integer[] r : results) {
			if (r != null) {
				saveNotes(r);
				break;
			}
		}
  }

  /**
   * Saves {@link Note}s
   *
   * @param The results to save
   */
  private void saveNotes(final Integer[] result) {

    saveTwenties(Optional.ofNullable(result[0]).orElse(0));
    saveFifties(Optional.ofNullable(result[1]).orElse(0));
  }

  /**
   * Saves twenty {@link Note}s
   *
   * @param The amount to save
   */
  private void saveTwenties(final Integer amount) {

    final Note twentyNote = noteRepository.findByNote(NOTE20);
    final int numberOfTwentiesAvailable = twentyNote.getAmount();
    twentyNote.setAmount(numberOfTwentiesAvailable - amount);
    noteRepository.save(twentyNote);
  }

  /**
   * Saves fifty {@link Note}s
   *
   * @param The amount to save
   */
  private void saveFifties(final Integer amount) {

    final Note fiftyNote = noteRepository.findByNote(NOTE50);
    final int numberOfFiftiesAvailable = fiftyNote.getAmount();
    fiftyNote.setAmount(numberOfFiftiesAvailable - amount);
    noteRepository.save(fiftyNote);
  }
}
