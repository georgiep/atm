package com.atm.boot.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.Getter;

/**
 * @author georgiep
 *     <p>Invalid note exception
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
@Getter
public class InvalidNotesException extends Exception {

  private static final long serialVersionUID = -5324911214441184576L;

  public InvalidNotesException(String message) {
    super(message);
  }
}
