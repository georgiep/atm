package com.atm.boot.exception;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.atm.boot.api.json.Result;
import com.atm.boot.utils.Helper;

/**
 * @author georgiep
 *     <p>Handles exceptions thrown by the dispenser api
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
@RestController
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(InvalidNotesException.class)
  public final ResponseEntity<Result> handleInvalidNotesException(
      InvalidNotesException ex, WebRequest request) {
    return buildResponseEntity(Helper.constructBadResult(ex.getMessage()));
  }

  private ResponseEntity<Result> buildResponseEntity(Result result) {
    return ResponseEntity.badRequest().body(result);
  }
}
