package com.atm.boot.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author georgiep
 *     <p>Note entity. Holds information about the dispenser's notes
 */
@Data
@Entity(name = "Note")
@Table(name = "NOTE")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"})
public class Note {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "NOTE_ID")
  private Integer id;

  @Column(name = "NOTE")
  private int note;

  @Column(name = "NOTE_AMOUNT")
  private int amount;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "TIMESTAMP")
  private Date timestamp;
}
