package com.atm.boot.entity;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

/** @author georgiep Note repository. Provides a repository for the {@link Note} entity */
public interface NoteRepository extends CrudRepository<Note, Long> {

  @Transactional(readOnly = true)
  public Note findByNote(final int note);

  @Transactional(readOnly = true)
  public List<Note> findAllByOrderByNoteAsc();
}
