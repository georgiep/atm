package com.atm.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DispenserApiApplication {

  public static void main(String[] args) {
    SpringApplication.run(DispenserApiApplication.class, args);
  }
}
