package com.atm.boot.api.json;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;

/**
 * @author georgiep
 *     <p>JSON results. Holds the actual {@link Data} and other information, such as the response
 *     timestamp, {@link HTTPStatus} and an {@link Optional} message
 */
@lombok.Data
@Builder
@AllArgsConstructor
public class Result {

  private Data data;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
  private LocalDateTime timestamp;

  private int status;
  private HttpStatus error;
  private Optional<String> message;
}
