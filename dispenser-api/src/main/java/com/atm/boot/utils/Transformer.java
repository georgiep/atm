package com.atm.boot.utils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.atm.boot.entity.Note;

/**
 * @author georgiep
 *     <p>A utility service that is utilized to transform notes
 */
@Service
public class Transformer {

  /**
   * Transforms from a {@link List} of {@link NotesDTO} to a {@link List} of {@link Note}
   *
   * @param notesDTO The {@link List} of {@link NotesDTO} to transform
   * @return The transformed {@link List} of {@link Note}
   */
  public List<Note> transformNotesDTOToEntity(final List<NotesDTO> notesDTO) {

    return Optional.ofNullable(notesDTO)
        .orElse(Collections.emptyList())
        .stream()
        .map(n -> Helper.buildNote(n))
        .collect(Collectors.toList());
  }

  /**
   * Transforms from a {@link List} of {@link Note}s to a {@link List} of {@link
   * com.atm.boot.api.json.Note}s
   *
   * @param notes The {@link List} of {@link Note}s to transform
   * @return The transformed {@link List} of {@link com.atm.boot.api.json.Note}s
   */
  public List<com.atm.boot.api.json.Note> transformNotesEntityToJson(final List<Note> notes) {

    return Optional.ofNullable(notes)
        .orElse(Collections.emptyList())
        .stream()
        .map(n -> Helper.buildNoteJson(n))
        .collect(Collectors.toList());
  }
}
