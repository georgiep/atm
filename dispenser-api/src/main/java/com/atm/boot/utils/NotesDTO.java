package com.atm.boot.utils;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author georgiep
 *     <p>A DTO that holds data regarding the {@link Note}s
 */
@Data
@NoArgsConstructor
public class NotesDTO {

  private int note;
  private int amount;
}
