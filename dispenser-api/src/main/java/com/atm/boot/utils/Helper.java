package com.atm.boot.utils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;

import com.atm.boot.api.json.Data;
import com.atm.boot.api.json.Result;
import com.atm.boot.entity.Note;

/**
 * @author georgiep
 *     <p>A utility class that provides helper methods
 */
public final class Helper {

  /**
   * Builds a @{@link HttpStatus} OK {@link Result}
   *
   * @param data The {@link Data} to build
   * @return The actual {@link Result}
   */
  public static Result constructOKResult(final Data data) {
    return buildResult(data, HttpStatus.OK, "");
  }

  /**
   * Builds a @{@link HttpStatus} bad request {@link Result}
   *
   * @param message The message to send
   * @return The actual {@link Result}
   */
  public static Result constructBadResult(final String message) {
    return buildResult(new Data(), HttpStatus.BAD_REQUEST, message);
  }

  /**
   * Builds a {@link Result}
   *
   * @param data The {@link Data} to build
   * @param status The actual {@link HttpStatus}
   * @param msg An {@link Optional} message to send
   * @return The actual {@link Result}
   */
  public static Result buildResult(final Data data, final HttpStatus status, final String msg) {
    return Result.builder()
        .data(data)
        .timestamp(LocalDateTime.now())
        .status(status.value())
        .error(status)
        .message(Optional.of(msg))
        .build();
  }

  /**
   * Builds a {@link Note} from a {@link NotesDTO}
   *
   * @param notesDTO The {@link NotesDTO} to build from
   * @return The build {@link Note}
   */
  public static Note buildNote(final NotesDTO notesDTO) {

    return Note.builder()
        .note(Optional.ofNullable(notesDTO.getNote()).orElse(0))
        .amount(Optional.ofNullable(notesDTO.getAmount()).orElse(0))
        .timestamp(getCurrentTimestamp())
        .build();
  }

  /**
   * Builds a {@link com.atm.boot.api.json.Note} from a {@link Note}
   *
   * @param note The {@link Note} to build from
   * @return The build {@link com.atm.boot.api.json.Note}
   */
  public static com.atm.boot.api.json.Note buildNoteJson(final Note note) {

    return new com.atm.boot.api.json.Note()
        .withNote(Optional.ofNullable(note.getNote()).orElse(0))
        .withAmount(Optional.ofNullable(note.getAmount()).orElse(0));
  }

  /**
   * Gets the current timestamp
   *
   * @return The current {@link Date}
   */
  private static Date getCurrentTimestamp() {
    return Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant());
  }

  /**
   * Recursively, calculates the number of notes to dispense. Source found <a href=
   * "https://stackoverflow.com/questions/22128759/atm-algorithm-of-giving-money-with-limited-amount-of-bank-notes">here</a>
   *
   * @param values The available notes
   * @param amounts The corresponding amounts of notes
   * @param variation The variation
   * @param price The price to withdraw
   * @param position The index position
   * @return The {@link List} of amount of notes to dispense
   */
  public static List<Integer[]> solutions(
      final int[] values,
      final int[] amounts,
      final int[] variation,
      final int price,
      final int position) {
    final List<Integer[]> list = new ArrayList<>();
    final int value = compute(values, variation);
    if (value < price) {
      for (int i = position; i < values.length; i++) {
        if (amounts[i] > variation[i]) {
          final int[] newvariation = variation.clone();
          newvariation[i]++;
          final List<Integer[]> newList = solutions(values, amounts, newvariation, price, i);
          if (newList != null) {
            list.addAll(newList);
          }
        }
      }
    } else if (value == price) {
      list.add(myCopy(variation));
    }
    return list;
  }

  /**
   * Computes the notes depending on the variation
   *
   * @param values The notes to used to computation
   * @param variation The variation to used to computation
   * @return The actual result
   */
  private static int compute(final int[] values, final int[] variation) {
    int ret = 0;
    for (int i = 0; i < variation.length; i++) {
      ret += values[i] * variation[i];
    }
    return ret;
  }

  /**
   * Copies the number of notes to dispense depending on the variation
   *
   * @param ar The variations
   * @return The number of notes to dispense
   */
  private static Integer[] myCopy(final int[] ar) {
    final Integer[] ret = new Integer[ar.length];
    for (int i = 0; i < ar.length; i++) {
      ret[i] = ar[i];
    }
    return ret;
  }
}
