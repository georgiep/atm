package com.atm.boot.utils;

import java.util.List;

import org.springframework.stereotype.Service;

import com.atm.boot.exception.InvalidNotesException;

/**
 * @author georgiep
 *     <p>Validates the notes requested to dispense
 */
@Service
public class Validator {

  private static final String INVALID_NOTE_MSG =
      "Please make sure that notes are valid (only positive numeric values allowed)";

  /**
   * Checks that the {@link NotesDTO} amount is valid
   *
   * @param notesDTO The {@link NotesDTO} to check
   * @throws InvalidNotesException
   */
  public void validateNotes(final List<NotesDTO> notesDTO) throws InvalidNotesException {
    if (notesDTO.isEmpty() || notesAmountInvalid(notesDTO))
      throw new InvalidNotesException(INVALID_NOTE_MSG);
  }

  /**
   * Checks that the {@link NotesDTO} is not empty and that the amount is greater than zero
   *
   * @param notesDTO The {@link NotesDTO} to check
   * @return If the {@link NotesDTO} is valid or not
   */
  private boolean notesAmountInvalid(final List<NotesDTO> notesDTO) {
    return notesDTO.stream().anyMatch(n -> n.getAmount() < 1 || n.getNote() < 1);
  }
}
