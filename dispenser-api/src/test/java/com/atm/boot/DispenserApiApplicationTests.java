package com.atm.boot;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DispenserApiApplicationTests {

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void testAsetUpDispenser() throws Exception {
		this.mockMvc
		.perform(post("/setup")
        .contentType(MediaType.APPLICATION_JSON)
        .content("[{\"note\": 50, \"amount\": 2}, {\"note\": 20, \"amount\": 5}]"))
        .andDo(print())
        .andExpect(status().isOk());
	}
	
	@Test
	public void testBsetUpDispenserInvalid() throws Exception {
		this.mockMvc
		.perform(post("/setup")
        .contentType(MediaType.APPLICATION_JSON)
        .content("[{\"note\": 50, \"amount\": 0}, {\"note\": 20, \"amount\": 0}]"))
        .andDo(print())
        .andExpect(status().isBadRequest())
        .andExpect(content().json("{\"status\":400,\"error\":\"BAD_REQUEST\"}"));
	}
	
	@Test
	public void testCgetBalanceReport() throws Exception {
		this.mockMvc
		.perform(get("/balance"))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(content().json("{\"data\":{\"balance\":200}}"));
	}
	
	@Test
	public void testDInvalidwithdraw() throws Exception {
		this.mockMvc
		.perform(post("/withdraw")
        .contentType(MediaType.APPLICATION_JSON)
        .content("{\"amount\": 30}"))
        .andDo(print())
        .andExpect(status().isOk());
	}
	
	@Test
	public void testEwithdraw() throws Exception {
		this.mockMvc
		.perform(post("/withdraw")
        .contentType(MediaType.APPLICATION_JSON)
        .content("{\"amount\": 110}"))
        .andDo(print())
        .andExpect(status().isOk());
	}
	
	@Test
	public void testFgetBalanceReport() throws Exception {
		this.mockMvc
		.perform(get("/balance"))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(content().json("{\"data\":{\"balance\":90}}"));
	}
	
	@Test
	public void testGwithdraw() throws Exception {
		this.mockMvc
		.perform(post("/withdraw")
        .contentType(MediaType.APPLICATION_JSON)
        .content("{\"amount\": 40}"))
        .andDo(print())
        .andExpect(status().isOk());
	}
	
	@Test
	public void testHgetBalanceReport() throws Exception {
		this.mockMvc
		.perform(get("/balance"))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(content().json("{\"data\":{\"balance\":50}}"));
	}
	
	@Test
	public void testIwithdraw() throws Exception {
		this.mockMvc
		.perform(post("/withdraw")
        .contentType(MediaType.APPLICATION_JSON)
        .content("{\"amount\": 50}"))
        .andDo(print())
        .andExpect(status().isOk());
	}
	
	@Test
	public void testKgetBalanceReport() throws Exception {
		this.mockMvc
		.perform(get("/balance"))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(content().json("{\"data\":{\"balance\":0}}"));
	}
	

	@Test
	public void contextLoads() {
	}
}
