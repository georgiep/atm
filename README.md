## A REST API for an ATM dispenser

Wanted to illustrate a microservices architecture for an ATM dispenser. In a container-orchestration system like Kubernetes, a load balancer would be created to expose the services provided by the _api gateway_ (_gateway-api_). The gateway will look up the dispenser (_dispenser-api_) via _service discovery_. Both apis would be exposed as _node ports_.

###Frameworks/Libraries utilized
**Spring Boot** To develop RESTful web services and package the application as a runnable jar, which includes an embedded tomcat server.

**Spring Data** As an umbrella that takes care of SQL databases and reduces the effort to use them.

**Sping Cloud** To utilize Hystrix as a circuit breaker.

**H2 Database Engine** In memory database.

**Lombok** To avoid writing boilerplate parts of code.

###How to install
1. Clone the repository: **git clone https://georgiep@bitbucket.org/georgiep/atm.git** (https) or **git clone git@bitbucket.org:georgiep/atm.git** (ssh)
2. Navigate to the atm _dispenser-api_ project
3. run: **mvn package && java -jar target/atm-dispenser-api-0.0.1-SNAPSHOT.jar** (replace mvn with ./mvnw if you do not have maven installed on your machine)
4. Open a new terminal and navigate to the atm _gateway-api_ project
5. run: **mvn package && java -jar target/atm-gateway-api-0.0.1-SNAPSHOT.jar** (replace mvn with ./mvnw if you do not have maven installed on your machine)
6. Go to http://localhost:8881/index.html ato set up the dispenser (note: zeros are not accepted)
7. After selecting to proceed the dipsenser is set up and you can start withdrawing money. You can go back to the set up page to reset up the dispenser