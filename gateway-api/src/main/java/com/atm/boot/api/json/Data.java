package com.atm.boot.api.json;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

/**
 * @author georgiep
 *     <p>Data holder JSON object
 */
@lombok.Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Data {

  private int balance;
  private List<Note> notes;
}
