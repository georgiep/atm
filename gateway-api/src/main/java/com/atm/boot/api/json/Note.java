package com.atm.boot.api.json;

/**
 * @author georgiep
 *     <p>JSON object that holds the note details
 */
@lombok.Data
public class Note {

  private int note;
  private int amount;

  public Note withNote(final int note) {
    this.note = note;
    return this;
  }

  public Note withAmount(final int amount) {
    this.amount = amount;
    return this;
  }
}
