package com.atm.boot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author georgiep
 *     <p>API Gateway application configuration
 */
@Configuration
public class GatewayApiAppConfig {

  @Autowired private RestTemplateBuilder restTemplateBuilder;

  @Bean
  public RestTemplate restTemplate() {
    final RestTemplate restTemplate = restTemplateBuilder.build();
    return restTemplate;
  }
}
