package com.atm.boot.utils;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author georgiep
 *     <p>A DTO that holds data regarding the withdrawal
 */
@Data
@NoArgsConstructor
public class WithdrawDTO {

  private int amount;
}
