package com.atm.boot.utils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;

import com.atm.boot.api.json.Data;
import com.atm.boot.api.json.Note;
import com.atm.boot.api.json.Result;

/**
 * @author georgiep
 *     <p>A utility class that provides helper methods
 */
public final class Helper {

  /**
   * Builds a {@link Result}
   *
   * @param data The {@link Data} to build
   * @param status The actual {@link HttpStatus}
   * @param msg An {@link Optional} message to send
   * @return The actual {@link Result}
   */
  public static Result buildResult(
      final Data data, final HttpStatus status, final Optional<String> msg) {
    return Result.builder()
        .data(data)
        .timestamp(LocalDateTime.now())
        .status(status.value())
        .error(status)
        .message(msg)
        .build();
  }

  /**
   * Builds a {@link List} of default {@link Note}s
   *
   * @return The built {@link List} of {@link Note}s
   */
  public static List<Note> buildDefaultNotes() {
    final Note twenties = new Note().withNote(20).withAmount(0);
    final Note fifties = new Note().withNote(50).withAmount(0);
    final List<Note> notes = new ArrayList<>();
    notes.add(twenties);
    notes.add(fifties);
    return notes;
  }
}
