package com.atm.boot.gateway.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.atm.boot.api.json.Result;
import com.atm.boot.utils.NotesDTO;
import com.atm.boot.utils.WithdrawDTO;

/**
 * @author georgiep
 *     <p>A rest controller that handles requests to the API.
 */
@RestController
public class GatewayController {

  private static final String HELLO_MSG = "Hello, from ATM API, this is the gateway service";

  @Autowired private GatewayService gatewayService;

  @RequestMapping(value = "/")
  public String ok() {

    return HELLO_MSG;
  }

  /**
   * Handles a post request that sets up the dispenser
   *
   * @param notesDTO The {@link List} of notes to setup
   * @return A {@link ResponseEntity} that holds the actual {@link Result}
   */
  @PostMapping(value = "/setup", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Result> setup(@RequestBody List<NotesDTO> notesDTO) {

    return ResponseEntity.ok().body(gatewayService.setup(notesDTO));
  }

  /**
   * Handles a post request to withdraw money from the dispenser
   *
   * @param withdrawDTO The amount to withdraw
   * @return A {@link ResponseEntity} that holds the actual {@link Result}
   */
  @PostMapping(value = "/withdraw", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Result> withdraw(@RequestBody WithdrawDTO withdrawDTO) {

    return ResponseEntity.ok().body(gatewayService.withdraw(withdrawDTO));
  }

  /**
   * Handles a get request to get the balance report from the dispenser
   *
   * @return A {@link ResponseEntity} that holds the actual {@link Result}
   */
  @RequestMapping(value = "/balance", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Result> getBalance() {

    return ResponseEntity.ok().body(gatewayService.getBalance());
  }
}
