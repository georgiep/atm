package com.atm.boot.gateway.service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.atm.boot.api.json.Data;
import com.atm.boot.api.json.Result;
import com.atm.boot.dispenser.json.DispenserResponse;
import com.atm.boot.utils.Helper;

/**
 * @author georgiep
 *     <p>A utility service that is utilized to transform from a dispenser response to the actual
 *     gateway api objects
 */
@Service
public class Transformer {

  /**
   * Transforms a {@link DispenserResponse} to the actual {@link Result}
   *
   * @param dispenserResponse The {@link DispenserResponse} received from the dispenser api
   * @return The transformed {@link Result}
   */
  public Result transformDispenserResponse(final DispenserResponse dispenserResponse) {

    return Helper.buildResult(
        transformData(
            Optional.ofNullable(dispenserResponse.getData())
                .orElse(new com.atm.boot.dispenser.json.Data())),
        dispenserResponse.getError(),
        dispenserResponse.getMessage());
  }

  /**
   * Transforms a {@link DispenserResponse}'s {@link com.atm.boot.dispenser.json.Data} to the
   * actual @{@link Result}'s {@link Data}
   *
   * @param data The {@link com.atm.boot.dispenser.json.Data} to transform
   * @return The transformed {@link Data}
   */
  private Data transformData(final com.atm.boot.dispenser.json.Data data) {

    return Data.builder()
        .balance(Optional.ofNullable(data.getBalance()).orElse(0))
        .notes(transformNotes(data.getNotes()))
        .build();
  }

  /**
   * Transforms a {@link DispenserResponse}'s {@link List} of {@link
   * com.atm.boot.dispenser.json.Note} to the actual @{@link Result}'s {@link Data} {@link
   * Note}s
   *
   * @param notes The {@link com.atm.boot.dispenser.json.Note}s to transform
   * @return The transformed {@link Note}s
   */
  private List<com.atm.boot.api.json.Note> transformNotes(
      final List<com.atm.boot.dispenser.json.Note> notes) {
    return Optional.ofNullable(notes)
        .orElse(Collections.emptyList())
        .stream()
        .map(
            n ->
                new com.atm.boot.api.json.Note()
                    .withNote(n.getNote())
                    .withAmount(n.getAmount()))
        .collect(Collectors.toList());
  }
}
