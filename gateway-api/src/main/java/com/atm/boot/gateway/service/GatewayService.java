package com.atm.boot.gateway.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atm.boot.api.json.Result;
import com.atm.boot.dispenser.service.BalanceService;
import com.atm.boot.dispenser.service.SetUpService;
import com.atm.boot.dispenser.service.WithdrawService;
import com.atm.boot.utils.NotesDTO;
import com.atm.boot.utils.WithdrawDTO;

/**
 * @author georgiep
 *     <p>The API gateway service decides which dispenser service to invoke based on the request
 */
@Service
public class GatewayService {

  @Autowired private SetUpService setupService;
  @Autowired private BalanceService balanceService;
  @Autowired private WithdrawService withdrawService;

  /**
   * Gets the actual balance from the dispenser, by utilizing the {@link BalanceService}
   *
   * @return The actual {@link Result}
   */
  public Result getBalance() {

    return balanceService.getResult();
  }

  /**
   * Withdraws money from the dispenser, by utilizing the {@link WithdrawService}
   *
   * @param withdrawDTO The amount to withdraw
   * @return The actual {@link Result}
   */
  public Result withdraw(final WithdrawDTO withdrawDTO) {

    withdrawService.setWithdrawDTO(withdrawDTO);
    return withdrawService.getResult();
  }

  /**
   * Sets up the dispenser by utilizing the {@link SetUpService}
   *
   * @param notesDTO The notes to setup
   * @return The actual {@link Result}
   */
  public Result setup(final List<NotesDTO> notesDTO) {

    setupService.setNotesDTO(notesDTO);
    return setupService.getResult();
  }
}
