package com.atm.boot.dispenser.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.NoArgsConstructor;

/**
 * @author georgiep
 *     <p>Data regarding the notes received from the dispenser response
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@lombok.Data
@NoArgsConstructor
public class Note {

  private int note;
  private int amount;

  public Note withNote(final int note) {
    this.note = note;
    return this;
  }

  public Note withAmount(final int amount) {
    this.amount = amount;
    return this;
  }
}
