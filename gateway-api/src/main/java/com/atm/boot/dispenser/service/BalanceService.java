package com.atm.boot.dispenser.service;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.atm.boot.api.json.Data;
import com.atm.boot.api.json.Result;
import com.atm.boot.dispenser.json.DispenserResponse;
import com.atm.boot.gateway.service.Transformer;
import com.atm.boot.utils.Helper;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

/**
 * @author georgiep
 *     <p>The balance services utilizes the dispenser api to get the balance report
 */
@Service
public class BalanceService extends DispenserService<Result> {

  private static final Logger LOGGER = Logger.getLogger(BalanceService.class.getName());

  private static final String BALANCE_PATH = "balance";

  @Autowired private RestTemplate restTemplate;
  @Autowired private Transformer transformer;

  /* (non-Javadoc)
   * @see com.cosmores.boot.dispenser.service.DispenserService#getResult()
   */
  @HystrixCommand(
    fallbackMethod = "reliable",
    commandProperties = {
      @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "3000")
    }
  )
  public Result getResult() {
    return transformer.transformDispenserResponse(getResponse());
  }

  /* (non-Javadoc)
   * @see com.cosmores.boot.dispenser.service.DispenserService#getResponse()
   */
  public DispenserResponse getResponse() {

    return restTemplate.getForObject(
        new StringBuilder("http://")
            .append(dispenserHost)
            .append(":")
            .append(dispenserPort)
            .append(BALANCE_PATH)
            .toString(),
        DispenserResponse.class);
  }

  /* (non-Javadoc)
   * @see com.cosmores.boot.dispenser.service.DispenserService#reliable(java.lang.Throwable)
   */
  public Result reliable(final Throwable e) {
    LOGGER.log(
        Level.WARNING,
        "reliable dispenser balance response has been called, exception: {0}",
        e.getMessage());

    return Helper.buildResult(
        Data.builder().balance(0).notes(Helper.buildDefaultNotes()).build(),
        HttpStatus.INTERNAL_SERVER_ERROR,
        Optional.of(new StringBuilder("An error occured, please try again").toString()));
  }
}
