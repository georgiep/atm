package com.atm.boot.dispenser.service;

import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.atm.boot.api.json.Data;
import com.atm.boot.api.json.Result;
import com.atm.boot.dispenser.json.DispenserResponse;
import com.atm.boot.gateway.service.Transformer;
import com.atm.boot.utils.Helper;
import com.atm.boot.utils.NotesDTO;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

@Service
public class SetUpService extends DispenserService<Result> {

  private static final Logger LOGGER = Logger.getLogger(SetUpService.class.getName());

  private static final String SET_UP_PATH = "setup";

  @Autowired private RestTemplate restTemplate;
  @Autowired private Transformer transformer;

  private List<NotesDTO> notesDTO;

  /* (non-Javadoc)
   * @see com.cosmores.boot.dispenser.service.DispenserService#getResult()
   */
  @HystrixCommand(
    fallbackMethod = "reliable",
    commandProperties = {
      @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "3000")
    }
  )
  public Result getResult() {
    return transformer.transformDispenserResponse(getResponse());
  }

  /* (non-Javadoc)
   * @see com.cosmores.boot.dispenser.service.DispenserService#getResponse()
   */
  public DispenserResponse getResponse() {

    return restTemplate.postForObject(
        new StringBuilder("http://")
            .append(dispenserHost)
            .append(":")
            .append(dispenserPort)
            .append(SET_UP_PATH)
            .toString(),
        notesDTO,
        DispenserResponse.class);
  }

  /* (non-Javadoc)
   * @see com.cosmores.boot.dispenser.service.DispenserService#reliable(java.lang.Throwable)
   */
  public Result reliable(Throwable e) {
    LOGGER.log(
        Level.WARNING,
        "reliable dispenser setup response has been called, exception: {0}",
        e.getMessage());

    return Helper.buildResult(
        new Data(),
        HttpStatus.INTERNAL_SERVER_ERROR,
        Optional.of(
            new StringBuilder("An error occured, please try again. ")
                .append("Notes: ")
                .append(Optional.ofNullable(notesDTO.toString()).orElse("not found"))
                .toString()));
  }

  public void setNotesDTO(final List<NotesDTO> notesDTO) {
    this.notesDTO = notesDTO;
  }
}
