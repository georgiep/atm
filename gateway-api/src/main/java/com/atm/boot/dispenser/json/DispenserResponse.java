package com.atm.boot.dispenser.json;

import java.util.Optional;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

/**
 * @author georgiep
 *     <p>Holds data regarding the dispenser response
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@lombok.Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DispenserResponse {
  private Data data;
  private int status;
  private HttpStatus error;
  private Optional<String> message;
}
