package com.atm.boot.dispenser.json;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

/**
 * @author georgiep
 *     <p>Holds the actual data of the dispenser response
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@lombok.Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Data {

  private int balance;
  private List<Note> notes;
}
